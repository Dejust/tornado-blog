class Publication(object):
    def __init__(self):
        """
        :param id: integer
        :param title: string
        :param content: string
        :param tags: string[]
        :return:
        """
        self.id = None
        self.title = None
        self.content = None
        self.tags = []
        self.author_id = None

    def has_identity(self):
        return self.id is not None

    def to_json(self):
        return self.__dict__

    def load(self, data):
        self.id = data.get('id', self.id)
        self.title = data.get('title', self.title)
        self.content = data.get('content', self.content)
        self.tags = data.get('tags', self.tags)


class Author(object):
    def __init__(self):
        self.id = None
        self.auth_key = None

    def has_identity(self):
        return self.id is not None

    def to_json(self):
        return self.__dict__

    def load(self, data):
        self.id = data.get('id', self.id)
        self.auth_key = data.get('id', self.auth_key)