import json

from tornado import gen
from models import Publication, Author


class PublicationRepository(object):
    def __init__(self, database):
        self._db = database
        super(PublicationRepository, self).__init__()

    @gen.coroutine
    def find_one(self, identity):
        cursor = yield self._db.execute(
            'SELECT id, title, content, author_id, tags '
            'FROM publication '
            'WHERE publication.id = %s',
            [identity]
        )

        publication_data = cursor.fetchone()

        if publication_data:
            publication = self._create_publication_from_data(publication_data)
            raise gen.Return(publication)
        else:
            raise gen.Return(None)

    @gen.coroutine
    def find_all(self):
        cursor = yield self._db.execute('SELECT id, title, content, author_id, tags FROM publication')
        publications_data = cursor.fetchall()
        raise gen.Return(map(self._create_publication_from_data, publications_data))

    @gen.coroutine
    def put(self, publication):
        if publication.has_identity():
            publication = yield self._update(publication)
        else:
            publication = yield self._create(publication)

        raise gen.Return(publication)

    @gen.coroutine
    def _update(self, publication):
        yield self._db.execute(
            'UPDATE publication SET title=%s, content=%s, author_id=%s, tags=%s WHERE id=%s',
            [publication.title, publication.content, publication.author_id, json.dumps(publication.tags), publication.id]
        )

        raise gen.Return(publication)

    @gen.coroutine
    def _create(self, publication):
        cursor = yield self._db.execute(
            'INSERT INTO publication (title, content, author_id, tags) VALUES (%s, %s, %s, %s) RETURNING id',
            [publication.title, publication.content, publication.author_id, json.dumps(publication.tags)]
        )

        new_identity = cursor.fetchone()[0]
        publication.id = new_identity

        raise gen.Return(publication)

    @staticmethod
    def _create_publication_from_data(publication_data):
        publication = Publication()

        publication.id = publication_data[0]
        publication.title = publication_data[1]
        publication.content = publication_data[2]
        publication.author_id = publication_data[3]
        if publication_data[4]:
            publication.tags = publication_data[4]
        else:
            publication.tags = []

        return publication


class AuthorRepository(object):
    def __init__(self, database):
        self._db = database

    @gen.coroutine
    def put(self, author):
        if author.has_identity():
            yield self._update(author)
        else:
            yield self._create(author)

    @gen.coroutine
    def find_by_auth_key(self, auth_key):
        cursor = yield self._db.execute(
            'SELECT id, auth_key FROM author WHERE auth_key = %s',
            [auth_key]
        )

        row = cursor.fetchone()

        if row:
            author = Author()
            author.id = row[0]
            author.auth_key = row[1]
            raise gen.Return(author)
        else:
            raise gen.Return(None)

    @gen.coroutine
    def _update(self, author):
        yield self._db.execute(
            'UPDATE author SET auth_key = %s',
            [author.auth_key]
        )

    @gen.coroutine
    def _create(self, author):
        yield self._db.execute(
            'INSERT INTO author (auth_key) VALUES (%s)',
            [author.auth_key]
        )
