# Запуск приложения #
* docker-compose up db
* docker-compose run migrate
* docker-compose up web

# Запуск тестов #
* docker-compose run tests

# API #
* ```GET /publications```
* ```GET /publication/(id)```
* ```GET /auth/generate-key```
* ```POST /publication```
* ```PUT  /publication/(id)```

Методы POST и PUT ожидают в теле запроса данные в следующем формате
```
#!json

{
   "title": "...",
   "content": "...",
   "tags": ["...", "..."],
   "key": "..."
}
```
Где ```key``` - токен автора, который можно получить выполнив запрос ```GET /auth/generate-key```