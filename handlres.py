import json
import uuid
from tornado import gen
from tornado.web import RequestHandler, HTTPError
from models import Publication, Author


class BaseHandler(RequestHandler):
    def initialize(self, publication_repository, author_repository):
        self.publication_repository = publication_repository
        self.author_repository = author_repository

    @property
    def request_data(self):
        return json.loads(self.request.body)

    @gen.coroutine
    def get_current_user(self):
        auth_key = self.request_data.get('key', None)

        if not auth_key:
            raise gen.Return(None)
        author = yield self.author_repository.find_by_auth_key(auth_key)
        raise gen.Return(author)

    @gen.coroutine
    def check_access(self):
        author = yield self.get_current_user()
        if not author:
            raise gen.Return(False)
        raise gen.Return(True)


class PublicationHandler(BaseHandler):
    @gen.coroutine
    def post(self):
        current_user = yield self.get_current_user()
        if not current_user:
            raise HTTPError(403)

        publication = Publication()
        publication.load(self.request_data)
        publication.author_id = current_user.id

        publication = yield self.publication_repository.put(publication)

        self._write_response(publication)

    @gen.coroutine
    def put(self, id):
        current_user = yield self.get_current_user()
        if not current_user:
            raise HTTPError(403)

        publication = yield self.publication_repository.find_one(id)
        if not publication:
            raise HTTPError(404)

        if publication.author_id != current_user.id:
            raise HTTPError(403)

        publication.load(self.request_data)

        yield self.publication_repository.put(publication)

        self._write_response(publication)

    @gen.coroutine
    def get(self, id):
        publication = yield self.publication_repository.find_one(id)
        if not publication:
            raise HTTPError(404)

        self._write_response(publication)

    def _write_response(self, publication):
        response = json.dumps({
            'item': publication
        }, default=complex_encoder)

        self.write(response)


class PublicationsListHandler(BaseHandler):
    @gen.coroutine
    def get(self):
        result = yield self.publication_repository.find_all()

        self.write({
            'items': map(lambda r: r.__dict__, result)
        })


class AuthKeyHandler(BaseHandler):
    @gen.coroutine
    def get(self):
        author = Author()
        author.auth_key = str(uuid.uuid4())

        yield self.author_repository.put(author)

        self.write({
            'auth_key': author.auth_key
        })


def complex_encoder(o):
    if hasattr(o, 'to_json'):
        return o.to_json()
    else:
        raise TypeError, 'Object of type %s with value of %s is not JSON serializable' % (type(o), repr(o))
