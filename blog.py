from momoko import Pool
from tornado import ioloop, httpserver
from tornado.web import Application

import config
from handlres import PublicationHandler, PublicationsListHandler, AuthKeyHandler
from repositories import PublicationRepository, AuthorRepository


def make_app(database, **settings):
    handler_params = dict(
        publication_repository=PublicationRepository(database),
        author_repository=AuthorRepository(database)
    )

    handlers = [
        (r'/publication', PublicationHandler, handler_params),
        (r'/publication/(\d+)', PublicationHandler, handler_params),
        (r'/publications', PublicationsListHandler, handler_params),
        (r'/auth/generate-key', AuthKeyHandler, handler_params)
    ]

    return Application(handlers, **settings)


def main():

    loop = ioloop.IOLoop.instance()

    database = Pool(dsn=config.database.get('dsn'), ioloop=loop)

    future = database.connect()
    loop.add_future(future, callback=lambda f: loop.stop())
    loop.start()
    future.result()

    app = make_app(database, **config.application)

    server = httpserver.HTTPServer(app)
    server.listen(config.server.get('port'), config.server.get('address'))

    loop.start()


if __name__ == '__main__':
    main()
