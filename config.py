server = {
    'address': '0.0.0.0',
    'port': 8080
}

database = {
    'dsn': 'dbname=postgres user=postgres password=root host=db port=5432',
}

test_database = {
    'dsn': 'dbname=postgres user=postgres password=root host=db port=5432'
}

application = {
    'debug': True
}