import json
import tornado
from momoko import Pool
from tornado.testing import AsyncHTTPTestCase
import blog
import config
import psycopg2


class TestBlogApplication(AsyncHTTPTestCase):
    def get_app(self):
        self.database = Pool(dsn=config.test_database.get('dsn'))

        settings = {
            'debug': True
        }

        self.database.connect()

        return blog.make_app(self.database, **settings)

    def setUp(self):
        super(TestBlogApplication, self).setUp()
        self._db = psycopg2.connect(dsn=config.test_database.get('dsn'))

    def tearDown(self):
        cursor = self._db.cursor()

        cursor.execute('DELETE FROM publication')
        cursor.execute('DELETE FROM author')

        self._db.commit()

    def get_new_ioloop(self):
        return tornado.ioloop.IOLoop.instance()

    def test_get_publications(self):
        publications = [
            self._create_raw_publication(1000, 'tt', 'tc', ['t1', 't2'], 1),
            self._create_raw_publication(1001, 'tt2', 'tc2', ['t12', 't22'], 2)
        ]

        response = self.fetch('/publications')
        self.assertEqual(response.code, 200)

        expected_response = {'items': publications}
        actual_response = json.loads(response.body)
        self.assertEqual(expected_response, actual_response)

    def test_get_publication(self):
        publication = self._create_raw_publication(1, 'tt', 'tc', ['t1', 't2', 't3'], 1)

        response = self.fetch('/publication/1')
        self.assertEqual(response.code, 200)

        expected_response = {'item': publication}
        actual_response = json.loads(response.body)

        self.assertEqual(expected_response, actual_response)

    def test_get_not_exists_publication(self):
        response = self.fetch('/publication/42')

        self.assertEqual(response.code, 404)

    def test_post_publication(self):
        author = self._create_raw_author(1, '123')

        def create_publication():
            request = {
                'title': 'title',
                'content': 'content',
                'tags': ['123', '321', '123'],
                'key': author.get('auth_key')
            }

            response = self.fetch('/publication', method='POST', body=json.dumps(request))
            self.assertEqual(response.code, 200)
            return json.loads(response.body).get('item')

        def fetch_publication(pub_id):
            response = self.fetch('/publication/%s' % pub_id)
            self.assertEqual(response.code, 200)
            return json.loads(response.body).get('item')

        created_publication = create_publication()
        actual_publication = fetch_publication(created_publication.get('id'))

        self.assertEqual(created_publication, actual_publication)

    def test_post_publication_with_invalid_auth_key(self):
        request = json.dumps({
            'title': 'post_test_title',
            'content': 'post_test_title',
            'tags': ["tag11", "tag22"],
        })
        response = self.fetch('/publication', method='POST', body=request)
        self.assertEqual(response.code, 403)

    def test_put_not_exists_publication(self):
        author = self._create_raw_author(1, '123')

        request = json.dumps({
            'title': 'new_title',
            'content': 'new_content',
            'tags': ['new_tags'],
            'key': author.get('auth_key')
        })
        response = self.fetch('/publication/42', method='PUT', body=request)
        self.assertEqual(response.code, 404)

    def test_put_publication_with_invalid_auth_key(self):
        request = json.dumps({
            'title': 'post_test_title',
            'content': 'post_test_title',
            'tags': ["tag11", "tag22"],
            'key': 'INVALID KEY'
        })
        response = self.fetch('/publication/42', method='PUT', body=request)
        self.assertEqual(response.code, 403)

    def test_put_publication(self):
        author = self._create_raw_author(1, '123')

        def create_publication():
            request = json.dumps({
                'title': 'current publication title',
                'content': 'current publication content',
                'tags': ['123', '456'],
                'key': author.get('auth_key')
            })

            response = self.fetch('/publication', method='POST', body=request)
            self.assertEqual(response.code, 200)
            return json.loads(response.body).get('item')

        def update_publication(pub_id):
            request = json.dumps({
                'title': 'new title',
                'content': 'new content',
                'tags': ['new', 'tags'],
                'key': author.get('auth_key')
            })

            response = self.fetch('/publication/%d' % pub_id, method='PUT', body=request)
            self.assertEqual(response.code, 200)
            return json.loads(response.body).get('item')

        def fetch_publication(pub_id):
            response = self.fetch('/publication/%d' % pub_id, method='GET')
            self.assertEqual(response.code, 200)
            return json.loads(response.body).get('item')

        created_publication = create_publication()
        updated_publication = update_publication(created_publication.get('id'))
        actual_publication = fetch_publication(created_publication.get('id'))

        self.assertEqual(updated_publication, actual_publication)

    def test_auth_generate_key(self):
        response = self.fetch('/auth/generate-key', method='GET')

        self.assertEqual(response.code, 200)
        self.assertIn('auth_key', json.loads(response.body))

    def _create_raw_publication(self, pub_id, title, content, tags, author_id):
        cursor = self._db.cursor()

        raw_publication = {
            'id': pub_id,
            'title': title,
            'content': content,
            'tags': tags,
            'author_id': author_id
        }

        values = [pub_id, title, content, json.dumps(tags), author_id]

        cursor.execute(
            'INSERT INTO publication (id, title, content, tags, author_id) VALUES (%s, %s, %s, %s, %s)',
            values
        )

        self._db.commit()

        return raw_publication

    def _create_raw_author(self, a_id, auth_key):
        cursor = self._db.cursor()

        values = [a_id, auth_key]

        cursor.execute(
            'INSERT INTO author (id, auth_key) VALUES (%s, %s)',
            values
        )

        self._db.commit()

        return {
            'id': a_id,
            'auth_key': auth_key
        }
