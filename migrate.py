from tornado.options import define, options
import psycopg2

import config


class DatabaseMigration(object):
    def __init__(self, db):
        self._db = db

    def migrate(self):
        cursor = self._db.cursor()

        cursor.execute(
            'CREATE TABLE publication ('
            'id SERIAL, '
            'title varchar(255), '
            'content text, '
            'author_id int, '
            'tags json '
            ')'
        )

        cursor.execute(
            'CREATE TABLE author ('
            'id SERIAL, '
            'auth_key varchar(255) unique '
            ')'
        )

        self._db.commit()


def main():
    define('use_test_database', default=False)
    options.parse_command_line()

    database_config = config.database if not options.use_test_database else config.test_database

    db = psycopg2.connect(database_config.get('dsn'))

    migration = DatabaseMigration(db)
    migration.migrate()

if __name__ == '__main__':
    main()